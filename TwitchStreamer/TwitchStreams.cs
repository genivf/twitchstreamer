namespace TwitchStreamer {
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web.Script.Serialization;

    public static class TwitchStreams {
        private static readonly Regex Qualities = new Regex("NAME=\"([a-zA-Z) ]+)\"");
        private static readonly Regex Streams = new Regex(@"http(|s):\/\/(.*)");

        public static async Task<List<StreamDetails>> GetAllStreams(string channel) {
            string data;
            try {
                using (var http = new HttpClient()) {
                    var resp = await http.GetStringAsync($"http://api.twitch.tv/api/channels/{channel}/access_token");
                    var json = (Dictionary<string, object>) new JavaScriptSerializer().DeserializeObject(resp);
                    var auth = new {sig = json["sig"], token = Uri.EscapeUriString((string) json["token"])};
                    var url = $"http://usher.twitch.tv/api/channel/hls/{channel}.m3u8" +
                              "?player=twitchweb&allow_audio_only=true&allow_source=true&type=any&allow_spectre=false" +
                              $"&token={auth.token}&sig={auth.sig}&p={DateTime.Now.Millisecond}";
                    data = await http.GetStringAsync(url);
                }
            }
            catch (Exception e) {
                throw new NoResponseException(channel, e);
            }

            var qualities = Qualities.Matches(data);
            var streams = Streams.Matches(data);

            if (qualities.Count == 0 || (qualities.Count != streams.Count)) {
                throw new NoStreamFoundException(channel);
            }

            var list = new List<StreamDetails>();
            for (var i = 0; i < streams.Count; i++) {
                if (!Uri.IsWellFormedUriString(streams[i].Value, UriKind.Absolute)) {
                    throw new MalformUrlException();
                }
                list.Add(new StreamDetails {
                    Quality = qualities[i].Value.ToLower().Replace("name=", "").Replace("\"", ""),
                    Url = streams[i].Value
                });
            }

            return list;
        }

        public class StreamDetails {
            public string Quality { get; set; }
            public string Url { get; set; }
        }
    }
}
