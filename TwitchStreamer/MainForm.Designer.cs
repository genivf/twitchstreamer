﻿namespace TwitchStreamer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.inputPanel = new System.Windows.Forms.Panel();
            this.inputTextbox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.historyListbox = new System.Windows.Forms.ListBox();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.findPlayerButton = new System.Windows.Forms.Button();
            this.argsTextbox = new System.Windows.Forms.TextBox();
            this.playerTextbox = new System.Windows.Forms.TextBox();
            this.inputPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.settingsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputPanel
            // 
            this.inputPanel.Controls.Add(this.inputTextbox);
            this.inputPanel.Controls.Add(this.startButton);
            this.inputPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.inputPanel.Location = new System.Drawing.Point(0, 0);
            this.inputPanel.Name = "inputPanel";
            this.inputPanel.Size = new System.Drawing.Size(292, 32);
            this.inputPanel.TabIndex = 0;
            // 
            // inputTextbox
            // 
            this.inputTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputTextbox.Location = new System.Drawing.Point(3, 3);
            this.inputTextbox.Name = "inputTextbox";
            this.inputTextbox.Size = new System.Drawing.Size(230, 20);
            this.inputTextbox.TabIndex = 1;
            this.inputTextbox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.inputTextbox_KeyUp);
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(239, 3);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(50, 20);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.startButton_MouseClick);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.historyListbox);
            this.mainPanel.Controls.Add(this.settingsPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 32);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(292, 494);
            this.mainPanel.TabIndex = 1;
            // 
            // historyListbox
            // 
            this.historyListbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyListbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.historyListbox.ColumnWidth = 75;
            this.historyListbox.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.historyListbox.ItemHeight = 17;
            this.historyListbox.Location = new System.Drawing.Point(0, 0);
            this.historyListbox.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.historyListbox.MultiColumn = true;
            this.historyListbox.Name = "historyListbox";
            this.historyListbox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.historyListbox.Size = new System.Drawing.Size(292, 425);
            this.historyListbox.TabIndex = 1;
            this.historyListbox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.historyListbox_KeyUp);
            this.historyListbox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.historyListbox_MouseDoubleClick);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.findPlayerButton);
            this.settingsPanel.Controls.Add(this.argsTextbox);
            this.settingsPanel.Controls.Add(this.playerTextbox);
            this.settingsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.settingsPanel.Location = new System.Drawing.Point(0, 438);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(292, 56);
            this.settingsPanel.TabIndex = 0;
            // 
            // findPlayerButton
            // 
            this.findPlayerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findPlayerButton.Location = new System.Drawing.Point(259, 6);
            this.findPlayerButton.Name = "findPlayerButton";
            this.findPlayerButton.Size = new System.Drawing.Size(30, 20);
            this.findPlayerButton.TabIndex = 2;
            this.findPlayerButton.Text = "...";
            this.findPlayerButton.UseVisualStyleBackColor = true;
            this.findPlayerButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.findPlayerButton_MouseClick);
            // 
            // argsTextbox
            // 
            this.argsTextbox.Location = new System.Drawing.Point(3, 32);
            this.argsTextbox.Name = "argsTextbox";
            this.argsTextbox.Size = new System.Drawing.Size(286, 20);
            this.argsTextbox.TabIndex = 1;
            // 
            // playerTextbox
            // 
            this.playerTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playerTextbox.Location = new System.Drawing.Point(3, 6);
            this.playerTextbox.Name = "playerTextbox";
            this.playerTextbox.Size = new System.Drawing.Size(250, 20);
            this.playerTextbox.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 526);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.inputPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "TwitchStreamer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.inputPanel.ResumeLayout(false);
            this.inputPanel.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel inputPanel;
        private System.Windows.Forms.TextBox inputTextbox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.ListBox historyListbox;
        private System.Windows.Forms.Panel settingsPanel;
        private System.Windows.Forms.Button findPlayerButton;
        private System.Windows.Forms.TextBox argsTextbox;
        private System.Windows.Forms.TextBox playerTextbox;
    }
}

