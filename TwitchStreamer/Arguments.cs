namespace TwitchStreamer {
    using System.Linq;

    internal class Arguments {
        public string Player { get; private set; }
        public string PlayerArgs { get; private set; }
        public string Stream { get; private set; }
        public bool ShowChat { get; private set; }

        public static Arguments Parse(string[] args) {
            var opts = new Arguments {PlayerArgs = "{0}"};

            for (var i = 0; i < args.Length; i++) {
                switch (args[i]) {
                case "-p":
                    if (args.Length > i + 1) {
                        opts.Player = args[i++];
                    }
                    break;
                case "-a":
                    if (args.Length > i + 1) {
                        opts.PlayerArgs = args[i++];
                    }
                    break;
                case "-c":
                    opts.ShowChat = true;
                    break;
                default:
                    var stream = args[i];
                    if (stream.Contains('/')) {
                        stream = stream.Split('/').Last();
                    }
                    opts.Stream = stream;
                    break;
                }
            }

            return opts;
        }
    }
}