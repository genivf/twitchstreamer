namespace TwitchStreamer {
    using System;

    public class NoStreamFoundException : Exception {
        public NoStreamFoundException(string channel) : base(channel) {}
    }

    public class NoResponseException : Exception {
        public NoResponseException(string channel, Exception ex) : base(channel, ex) {}
    }

    public class MalformUrlException : Exception {}
}
