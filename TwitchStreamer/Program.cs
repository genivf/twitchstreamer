﻿namespace TwitchStreamer {
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Properties;
    using Microsoft.Win32;
    internal static class Program {
        [STAThread]
        private static void Main(string[] args) {
            if (args.Length > 0) {
                var opts = Arguments.Parse(args);
                try {
                    Handle(opts).Wait();
                }
                catch (Exception ex) {
                    MessageBox.Show($"Cannot start stream: {opts.Stream}\n{ex.Message}",
                                    Resources.ErrorNoStream,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            else {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
        }

        private static bool UpdateSettings(Arguments args) {
            Settings.Default.Args = string.IsNullOrWhiteSpace(args.PlayerArgs) ? "{0}" : args.PlayerArgs;

            if (!string.IsNullOrWhiteSpace(args.Player)) {
                Settings.Default.Player = args.Player;
            }

            if (string.IsNullOrWhiteSpace(Settings.Default.Player)) {
                var name = AppDomain.CurrentDomain.FriendlyName;
                MessageBox.Show($"run {name} without parameters.\nor use '{name} -p \"path\\to\\player.exe\"'",
                                Resources.NoPlayerTitle,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }

            if (Settings.Default.History == null) {
                Settings.Default.History = new StringCollection();
            }

            return true;
        }

        private static async Task Handle(Arguments args) {
            if (!UpdateSettings(args)) {
                return;
            }

            var streams = await TwitchStreams.GetAllStreams(args.Stream);
            var stream = streams.First(s => s.Quality == "source");
            await Task.Run(() => {
                var param = string.Format(Settings.Default.Args, stream.Url);
                return Process.Start(Settings.Default.Player, param);
            });

            if (!Settings.Default.History.Contains(args.Stream.ToLower())) {
                Settings.Default.History.Add(args.Stream);
            }
            Settings.Default.Save();

            if (args.ShowChat) {                
                Process.Start("chrome.exe", "--app="+ $"\"data:text/html,<html><body><script>window.moveTo(580,240);window.resizeTo(300,600);window.location='https://www.twitch.tv/{args.Stream.ToLower()}/chat?popout=';</script></body></html>\"");
            }
        }
    }
}
