﻿namespace TwitchStreamer {
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Properties;

    public partial class MainForm : Form {
        private readonly BindingList<string> history = new BindingList<string>();

        public MainForm() {
            InitializeComponent();
            historyListbox.DataSource = history;
        }

        private void inputTextbox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Enter) { return; }

            startButton_MouseClick(this, null);
            e.Handled = true;
        }

        private async void startButton_MouseClick(object sender, MouseEventArgs e) {
            if (string.IsNullOrWhiteSpace(inputTextbox.Text)) { return; }

            inputTextbox.Enabled = false;
            try {
                await Start(inputTextbox.Text);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, Resources.ErrorOccurred, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally {
                inputTextbox.Enabled = true;
                inputTextbox.Clear();
            }
        }

        private async void historyListbox_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                await Start((string) historyListbox.SelectedItem);
            }
        }

        private void historyListbox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) { return; }

            var remove = new List<int>();
            for (var i = historyListbox.SelectedIndices.Count - 1; i >= 0; i--) {
                remove.Add(historyListbox.SelectedIndices[i]);
            }
            foreach (var i in remove) {
                history.RemoveAt(i);
            }
            e.Handled = true;
        }

        private void findPlayerButton_MouseClick(object sender, MouseEventArgs e) {
            using (var fd = new OpenFileDialog {
                Title = Resources.FindPlayer,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
                Filter = Resources.ApplicationType,
                RestoreDirectory = true
            }) {
                if (fd.ShowDialog() == DialogResult.OK) {
                    playerTextbox.Text = fd.FileName;
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e) {
            playerTextbox.Text = Settings.Default.Player;
            argsTextbox.Text = Settings.Default.Args;

            if (Settings.Default.History?.Count > 0) {
                foreach (var entry in Settings.Default.History) {
                    history.Add(entry);
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            Settings.Default.Player = playerTextbox.Text;
            Settings.Default.Args = argsTextbox.Text;

            if (Settings.Default.History == null) {
                Settings.Default.History = new StringCollection();
            }

            Settings.Default.History.Clear();
            foreach (var entry in history) {
                Settings.Default.History.Add(entry);
            }

            Settings.Default.Save();
        }

        private async Task Start(string channel) {
            var stream = channel;
            if (channel.Contains('/')) {
                stream = channel.Split('/').Last();
            }

            if (!history.Contains(stream)) {
                history.Add(stream);
            }

            if (string.IsNullOrWhiteSpace(playerTextbox.Text)) {
                // notify no player set
                throw new Exception("no player found");
            }

            var player = playerTextbox.Text;
            var args = string.IsNullOrWhiteSpace(argsTextbox.Text) ? "{0}" : argsTextbox.Text;

            var streams = await TwitchStreams.GetAllStreams(stream);
            var url = streams.First(s => s.Quality == "source").Url;
            await Task.Run(() => Process.Start(player, string.Format(args, url)));
            historyListbox.SelectedIndex = history.IndexOf(stream);
        }
    }
}
